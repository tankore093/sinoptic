﻿using BlankWebApplication.DataAccess.Interfaces;
using BlankWebApplication.Models.Domain;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;



namespace BlankWebApplication.Controllers


{
    public class MessageController : Controller
    {
        private readonly IMessagesRepository _messageRepo;
        private readonly IConfiguration _config;
        private readonly IPubSub _pubSubRepo;
       public MessageController(IMessagesRepository messageRepo, IConfiguration config, IPubSub pubSubRepo)
        { 
            _messageRepo = messageRepo;
            _config = config;
            _pubSubRepo = pubSubRepo;
        }
        public IActionResult Index()
        {
            var list = _messageRepo.GetMessages();
            return View(list);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(IFormFile logo, Message m)
        {

            try
            {
                string bucketName = _config.GetSection("AppSettings").GetSection("BucketName").Value;
                string uniqueFilename = Guid.NewGuid() + System.IO.Path.GetExtension(logo.FileName);

                //upload a file in the cloud storage
                var storage = StorageClient.Create();

                using (var myStream = logo.OpenReadStream())
                {
                    storage.UploadObject(bucketName, uniqueFilename, null, myStream);
                }

                //add a reference to the file url with the instance of the blog >>> b

                m.Url = $"https://storage.googleapis.com/{bucketName}/{uniqueFilename}";

                _messageRepo.AddMessage(m);

                string eamil_rec = HttpContext.User.Identity.Name;
                _pubSubRepo.PublishMessage(m, eamil_rec, "testing");


                TempData["message"] = $"Message{m.Url}Message could be made";
                return RedirectToAction("Index");
            }catch(Exception ex)
            {
                TempData["error"] = "Message couldn't be made";
                return View(_messageRepo);
            }
         }

        public IActionResult Delete(int id)
        {
            try
            {
                _messageRepo.DeleteMessage(id);
                TempData["message"] = $"Message was deleted successfully";
            }
            catch (Exception
            e)
            {
                TempData["error"] = "Message wasnt deleted successfully";
            }

            return RedirectToAction("Index");
        }
    }
}
