﻿using BlankWebApplication.DataAccess.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlankWebApplication.Controllers
{
    public class EmailController : Controller
    {
        private readonly IMessagesRepository _messageRepo;
        private readonly IConfiguration _config;
        private readonly IPubSub _pubSubRepo;
        public EmailController(IMessagesRepository messageRepo, IConfiguration config, IPubSub pubSubRepo)
        {
            _messageRepo = messageRepo;
            _config = config;
            _pubSubRepo = pubSubRepo;
        }
        public IActionResult Pull()
        {
            string ser = _pubSubRepo.pullMessage();

            if (ser == string.Empty) return View();

            dynamic mydm = JsonConvert.DeserializeObject(ser);
                string email = mydm.Email;
                string CONTET = mydm.Message.CONTET;
                string MessageID = mydm.Message.MessageID;
                string OWNER = mydm.Message.OWNER;
                string TITLE = mydm.Message.TITLE;
                string TO = mydm.Message.TO;
                string Url = mydm.Message.Url;
            


            return Content("EMAIL SENT "+ email + " CONTET: " + CONTET + " MessageID:" + MessageID + " OWNER: " + OWNER + " TITLE: " + TITLE + " TO: " + TO + " Url: " + Url + " ");
        }
    }
}
