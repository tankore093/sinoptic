﻿using BlankWebApplication.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlankWebApplication.DataAccess.Interfaces
{
    public interface IMessagesRepository
    {
        void AddMessage(Message b);

        Message GetMessage(int id);

        IQueryable<Message> GetMessages();

        void DeleteMessage(int id);

        void UpdateMessage(Message B);
    }
}
