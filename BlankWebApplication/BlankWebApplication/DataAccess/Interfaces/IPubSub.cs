﻿using BlankWebApplication.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlankWebApplication.DataAccess.Interfaces
{
    public interface IPubSub
    {
        void PublishMessage(Message b, string email,string owner);

        string pullMessage();
    }
}
