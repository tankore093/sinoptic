﻿using BlankWebApplication.Data;
using BlankWebApplication.DataAccess.Interfaces;
using BlankWebApplication.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlankWebApplication.DataAccess.Repositories
{
    public class MessagesRepository : IMessagesRepository
    {
        private readonly ApplicationDbContext _context;
        public MessagesRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddMessage(Message b)
        {
            _context.Messages.Add(b);
            _context.SaveChanges();
        }

        public void DeleteMessage(int id)
        {
            _context.Messages.Remove(GetMessage(id));
            _context.SaveChanges();
        }

        public Message GetMessage(int id)
        {
            return _context.Messages.SingleOrDefault(X => X.MessageID == id);
        }

        public IQueryable<Message> GetMessages()
        {
            return _context.Messages;
        }

        public void UpdateMessage(Message B)
        {
            throw new NotImplementedException();
        }
    }
}
