﻿using BlankWebApplication.DataAccess.Interfaces;
using BlankWebApplication.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Google.Protobuf;
using Grpc.Core;

namespace BlankWebApplication.DataAccess.Repositories
{
    public class PubSub : IPubSub
    {
        string projectId;

        public PubSub(IConfiguration config)
        {
            projectId = config.GetSection("AppSettings").GetSection("ProjectId").Value;
        }
        public void PublishMessage(Message b, string email,string owner)
        {
            TopicName topicName = TopicName.FromProjectTopic(projectId, "sincoptic-pubsub");

            Task<PublisherClient> t = PublisherClient.CreateAsync(topicName);
                t.Wait();
            PublisherClient publisher = t.Result ;


            var myOnTheFlyObject = new { Email = email, Message = b };

            String myOnTheFlyObject_SER = JsonConvert.SerializeObject(myOnTheFlyObject);

            var pubsubMessage = new PubsubMessage
            {
                // The data is any arbitrary ByteString. Here, we're using text.
                Data = ByteString.CopyFromUtf8(myOnTheFlyObject_SER),
                // The attributes provide metadata in a string-to-string dictionary.
                Attributes =
            {
                { "owner", owner }
            }
            };

            Task<string> t2 = publisher.PublishAsync(pubsubMessage);
            t2.Wait();
            string message = t2.Result;
            Console.WriteLine($"Published message {message}");
        }

        public string pullMessage()
        {
            SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription(projectId, "sincoptic-pubsub-sub");
            SubscriberServiceApiClient subscriberClient = SubscriberServiceApiClient.Create();
            int messageCount = 0; string text = "";
            try
            {
                // Pull messages from server,
                // allowing an immediate response if there are no messages.
                PullResponse response = subscriberClient.Pull(subscriptionName, returnImmediately: true, maxMessages: 1);
                // Print out each received message.
                if (response.ReceivedMessages.Count > 0)
                {

                    var msg = response.ReceivedMessages.FirstOrDefault();
                    if (msg != null)
                    {
                        text = msg.Message.Data.ToStringUtf8();
                    }

                    subscriberClient.Acknowledge(subscriptionName, new List<string>() { msg.AckId });


                }
            }
            catch (RpcException ex) when (ex.Status.StatusCode == StatusCode.Unavailable)
            {
                // UNAVAILABLE due to too many concurrent pull requests pending for the given subscription.
            }
            return text;
        }
    }
}
