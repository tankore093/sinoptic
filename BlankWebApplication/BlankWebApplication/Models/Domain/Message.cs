﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BlankWebApplication.Models.Domain
{
    public class Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageID { get; set; }
        public string Url { get; set; }

        public string TITLE { get; set; }
        public string CONTET { get; set; }
        public string OWNER { get; set; }
        public string TO { get; set; }

    }
}
